# Auth Example Node js

## Tecnologias

- Express
- Pg
- Bcrypt
- Postgres

## Pasos a seguir

1. Crear el proyecto con `npm init -y`
2. Intalar las dependencias `npm i express pg bcrypt`
3. Definimos el punto de entrada de la palicacion `app.js`
4. Creamos el archivo de configuracion de la base de datos `pool.js`

## Implementacion de CI y CD

1. Crear un repositorio en gitlab
2. Crear un archivo `.gitlab-ci.yml`
3. Crear un archivo `Dockerfile`
4. Configurar los runners de gitlab y las variables de entorno
