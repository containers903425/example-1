const envs = {
    development:{
        port: process.env.PORT,
        user: process.env.DB_USER,
        host: process.env.DB_HOST,
        database: process.env.DB_NAME,
        password: process.env.DB_PASSWORD,
        portDb: process.env.DB_PORT
    },
    production:{
        port: process.env.PORT,
        user: process.env.DB_USER,
        host: process.env.DB_HOST,
        database: process.env.DB_NAME,
        password: process.env.DB_PASSWORD,
        portDb: process.env.DB_PORT
    }
}


export function getEnv(){
    return envs[process.env.NODE_ENV] || envs.development
}
