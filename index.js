import app from './src/app.js'
import { getEnv } from './config/env.js'
import { pool } from './src/db/pool.js'
const {port} = getEnv()
app.listen(port, ()=>{
   try {
       pool.connect((err, client, realease)=>{
           if (err) return console.log(err)
           client.query('SELECT NOW()', (err, result)=>{

               console.log('Connected to database')
               console.log(`Server running on port ${port}`)
               console.log(result.rows)
           })
       })
   }catch (error) {
       console.log(error)
   }
})
