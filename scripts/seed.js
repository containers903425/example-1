import { dataUser } from './placeholder.js'
import bcrypt from 'bcrypt'
import {pool} from "../src/db/pool.js";

async function seedUsers(client){
    try {
        const queryString = `CREATE TABLE IF NOT EXISTS users(
            ID SERIAL PRIMARY KEY,
            name VARCHAR(50) NOT NULL,
            age INT NOT NULL,
            email VARCHAR(50) UNIQUE,
            password VARCHAR(255) NOT NULL
        )`

        const createTable = await client.query(queryString)
        console.log('Table created')
        const insertData = Promise.all(
            dataUser.map(async (user) =>{
                const hash = await bcrypt.hash(user.password, 10)
                return await client.query(
                    `INSERT INTO USERS(name, age, email, password) VALUES($1, $2, $3, $4) RETURNING *`,
                    [user.name, user.age, user.email, hash])
            })
        )
        console.log(`Data inserted: ${insertData.length}`)

        return{
            createTable,
            insertData
        }
    }catch (error) {
        console.log(error)
    }
}

async function seed(){
    try {
        await pool.connect(async (err, client)=>{
            if (err) return console.log(err)
            console.log('Connected to database')
            await seedUsers(client)
            console.log('Data seeded')
        })
    }catch (error) {
        console.log(error)
    }
}

seed()
