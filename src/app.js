import express from 'express'
import morgan from 'morgan'
import {pool} from "./db/pool.js";
import bcrypt from 'bcrypt'
const app = express()

app.use(morgan('dev'))

app.use(express.json())

app.get('/', (req, res)=>{
    res.send('Hello World')
})

app.post('/register', async (req, res)=>{
    try {
        const user = req.body
        const hash = await bcrypt.hash(user.password, 10)
        const resData = await  pool.query(`INSERT INTO USERS(name, age, email, password) VALUES($1, $2, $3, $4) RETURNING *`, [user.name, user.age, user.email, hash])

        return res.status(201).json(resData.rows[0])
    }catch (error) {
        return res.status(500).json({error: error.message})
    }
})

app.post('/login', async (req, res)=>{
    try {
        const data = req.body
        const emailExists =  await pool.query(`SELECT * FROM users WHERE email = $1`, [data.email])

        if (!emailExists) return res.status(400).json({error: 'Email not found'})
        const user = emailExists.rows[0]
        console.log(user)

        const validPassword = await bcrypt.compare(data.password, user.password)
        console.log(validPassword)
        if (!validPassword) return res.status(400).json({error: 'Invalid password'})
        return res.status(200).json({message: 'Login successful', data:{
            name: user.name,
            email: user.email,
        }})
    }catch (error) {
        return res.status(500).json({error: error.message})
    }
})

export default app

