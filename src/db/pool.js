import pg from 'pg';
import { getEnv } from '../../config/env.js'

const { user, host, database, password, portDb } = getEnv()
export const pool = new pg.Pool({
    user: user,
    host: host,
    database: database,
    password: password,
    port: portDb,
})

pool.query(`CREATE TABLE IF NOT EXISTS users(
            ID SERIAL PRIMARY KEY,
            name VARCHAR(50) NOT NULL,
            age INT NOT NULL,
            email VARCHAR(50) UNIQUE,
            password VARCHAR(255) NOT NULL)
          `)
